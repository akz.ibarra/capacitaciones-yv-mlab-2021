
[Regresar](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/blob/master/README.md)

# Menú
[Que es git](#Que-es-git) 

[Comandos de git en consola](#Comandos-de-git-en-consola)

[Clientes git](#Clientes-git)

[Clonación de proyecto por consola y por cliente](#Clonación-de-proyecto-por-consola-y-por-cliente)

[Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)

[Ramas de kraken](#Ramas-de-kraken)

[Merge](#Merge)

# Que es git

Es una herramienta que realiza una funcion del control de versiones de código de forma distribuida.


# Comandos de git en consola

Git init sirve para crear repositorios en la carpeta.

![Comando git init imagen](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20init.PNG)

Git status sirve para ver el estado de los archivos en el repositorio.

![Comando git status imagen](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20status.PNG)

Git add . se utiliza para guardar cambios a nustro git.

![Comando git add . imagen](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20add%20..PNG)


Git push se sube a la nube del git.

![Comando git push imagen](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20push.PNG)

Git pull utilizamos para bajar cambios desde gitlab,

![Comando git pull imagen](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20pull.PNG)

# Clientes git

La interfaz para clientes git son: GitBash y Kraken

![Cliente git](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/clientes%20git.PNG)

# Clonación de proyecto por consola y por cliente

Git clone sirve para clonar un repositorio.

![Comando git clone imagen](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20clone.PNG)

Para clonar por cliente debemos ingresar la URL del repositorio y la ruta donde deseemos clonar.

![Git clone por kraken](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/clonar%20por%20cliente.PNG)


# Commits por consola y por cliente kraken o smart

Commit por consola guardar cambios para subir a gitlab.

![Commit por consola](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/git%20commit%20-m%20consola.PNG)

Commit por cliente kraken 

![Commit kraken](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/e2642200b63022165a2fbd73c7d754b8e621ba8d/Imagenes/commit%20por%20kraken.PNG)

# Ramas de kraken

Para la creacion de ramas por kraken debemos darle click en branch.

![Ramas kraken](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/ramas%20kraken.PNG)

# Merge

Integra las subramas a las ramas.

![Merge kraken](https://gitlab.com/akz.ibarra/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/merge%20kraken.PNG)
